from flask import Flask, Response, render_template, request
import json
from subprocess import Popen, PIPE
import os
from tempfile import mkdtemp
from werkzeug import secure_filename
import requests
from urllib import urlopen
from json import loads
import boto3


app = Flask(__name__)

template = """
<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <style>body { margin: 20px; } </style>
</head>
<body>
	SQS Example API using FLASK
</body>
</html>
"""

@app.route('/')
def index():
  return template 

# EXAMPLE USE
# curl -s -X GET -H 'Content-type: application/json'  https://web.134vm.nightsky.ie/queues | jq

@app.route('/queues', methods=['GET'])
def queues():
  sqs  = boto3.client('sqs',region_name="ap-northeast-2")

  # Get a list of the queues that exists and then print the list out
  response = sqs.list_queues()

  # Convert the list to JSON format
  resp = json.dumps(convert_to_json(response['QueueUrls']))
  return Response(response=resp, mimetype="application/json")
      

# EXAMPLE USE
# curl -s -X GET -H 'Content-type: application/json'  https://web.134vm.nightsky.ie/consumemsg | jq

@app.route('/consumemsg', methods=['GET'])
def consumemsg():
  sqs  = boto3.client('sqs',region_name="ap-northeast-2")


  # Convert the list to JSON format
  resp = ''
  return Response(response=resp, mimetype="application/json")
 
# EXAMPLE USE
# curl -s -X GET -H 'Content-type: application/json'  https://web.134vm.nightsky.ie/countmsgs | jq

@app.route('/countmsgs', methods=['GET'])
def countmsgs():
  sqs  = boto3.client('sqs',region_name="ap-northeast-2")

  # Convert the list to JSON format
  resp = ''
  return Response(response=resp, mimetype="application/json")

# EXAMPLE USE
# curl -s -X POST -H 'Content-type: application/json'  https://web.134vm.nightsky.ie/msg -d '{"msg":"your message"}'| jq

@app.route('/msg', methods=['POST'])
def msg():
  sqs  = boto3.client('sqs',region_name="ap-northeast-2")


  # Convert the list to JSON format
  resp = ''
  return Response(response=resp, mimetype="application/json")      

# Useful function to help create list of JSON.DUMPS
def convert_to_json(urllist):
	all = []
	each = {}
	each['Queue_url'] = urllist
	all.append(each)
	return all
  
if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0', port=5000)

